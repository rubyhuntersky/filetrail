extern crate clap;
extern crate serde;
extern crate serde_derive;
extern crate serde_json;
extern crate toml;
extern crate warp;

use std::error::Error;

use simple_logger::SimpleLogger;

use commands::app;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
	SimpleLogger::new().init()?;

	let matches = app::command().get_matches();
	app::run(&matches).await?;
	Ok(())
}

mod commands;
mod settings;
mod core;