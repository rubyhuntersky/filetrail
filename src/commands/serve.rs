use std::error::Error;
use std::sync::mpsc::{channel, Sender, sync_channel, SyncSender};
use std::thread;

use clap::{ArgMatches, Command};
use warp::Filter;
use warp::http::StatusCode;

use crate::commands::{trail_data_path, trail_path};
use crate::core::{FileTrail, TrailEnd, TrailError};

pub const COMMAND_NAME: &str = "serve";

pub fn command() -> Command<'static> {
	Command::new(COMMAND_NAME)
		.about("Launch a daemon to provide REST access to the file.")
}

pub async fn run(_args: &ArgMatches) -> Result<(), Box<dyn Error>> {
	let host_port = ([0, 0, 0, 0], 2626);
	eprintln!("LINK: http://{}:{}/trail", host_port.0.iter().map(|x| x.to_string()).collect::<Vec<_>>().join("."), host_port.1);

	let client = Client::connect();
	let read = {
		let client = client.clone();
		warp::path!("trail").and(warp::get())
			.map(move || {
				let end = client.read();
				warp::reply::json(&end)
			})
	};
	let write = {
		let client = client.clone();
		warp::path!("trail" ).and(warp::post())
			.and(warp::body::json())
			.map(move |end: TrailEnd| {
				match client.write(&end) {
					Ok(height) => {
						warp::reply::with_status(format!("{}", height), StatusCode::OK)
					}
					Err(e) => {
						warp::reply::with_status(e.to_string(), StatusCode::CONFLICT)
					}
				}
			})
	};
	let options = warp::path("trail").and(warp::options())
		.map(move || {
			warp::reply::with_status("Ok".to_string(), StatusCode::OK)
		});

	let cors = warp::cors()
		.allow_any_origin()
		.allow_methods(vec!["POST", "GET", "OPTIONS"])
		.allow_headers(vec!["Content-Type"]);

	let log = warp::log("filetrail-serve");
	let routes = read.or(write).or(options).with(cors).with(log);
	warp::serve(routes).run(host_port).await;
	Ok(())
}

pub enum Msg {
	Read(Sender<TrailEnd>),
	Write(TrailEnd, Sender<Result<usize, TrailError>>),
}

#[derive(Clone)]
pub struct Client {
	sender: SyncSender<Msg>,
}

impl Client {
	pub fn read(&self) -> TrailEnd {
		let (send, recv) = channel();
		self.sender.send(Msg::Read(send)).expect("send read");
		recv.recv().expect("read result")
	}
	pub fn write(&self, trail_end: &TrailEnd) -> Result<usize, TrailError> {
		let (send, recv) = channel();
		self.sender.send(Msg::Write(trail_end.clone(), send)).expect("send write");
		recv.recv().expect("write result")
	}

	pub fn connect() -> Self {
		let (send, recv) = sync_channel(100);
		thread::spawn(move || {
			let trail_data_path = {
				let trail_path = trail_path();
				trail_data_path(&trail_path)
			};
			let mut trail = FileTrail::load(&trail_data_path).expect("load");
			for msg in recv {
				match msg {
					Msg::Read(sender) => {
						let end = trail.read().clone();
						if sender.send(end).is_err() {
							break;
						}
					}
					Msg::Write(end, sender) => {
						let result = trail.write(end.height, end.content);
						if sender.send(result).is_err() {
							break;
						}
					}
				}
			}
		});
		Client { sender: send }
	}
}


