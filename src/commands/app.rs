use std::error::Error;

use clap::{ArgMatches, Command};

use crate::commands::{init, serve};

pub const COMMAND_NAME: &str = "filetrail";

pub fn command() -> Command<'static> {
	Command::new(COMMAND_NAME)
		.about("Track a single file through time.")
		.arg_required_else_help(true)
		.subcommand_required(true)
		.subcommand(init::command())
		.subcommand(serve::command())
}

pub async fn run(args: &ArgMatches) -> Result<(), Box<dyn Error>> {
	match args.subcommand() {
		Some((init::COMMAND_NAME, sub_matches)) => init::run(&sub_matches)?,
		Some((serve::COMMAND_NAME, sub_matches)) => serve::run(&sub_matches).await?,
		_ => unreachable!("Subcommand_required prevents `None`"),
	}
	Ok(())
}

