use std::path::{Path, PathBuf};

pub mod app;
pub mod init;
pub mod serve;

pub fn trail_path() -> PathBuf {
	Path::new(".").to_path_buf()
}

pub fn trail_settings_path(trail_path: &Path) -> PathBuf {
	trail_path.join("FileTrail.toml")
}

pub fn trail_data_path(trail_path: &Path) -> PathBuf {
	trail_path.join("filetrail_data")
}


