use std::error::Error;
use std::fs;
use std::fs::OpenOptions;
use std::io::Write;

use clap::{ArgMatches, Command};

use crate::commands::{trail_data_path, trail_path, trail_settings_path};
use crate::settings::Settings;

pub const COMMAND_NAME: &str = "init";

pub fn command() -> Command<'static> {
	Command::new(COMMAND_NAME)
		.about("Initialize a file trail in the current directory.")
}

pub fn run(_args: &ArgMatches) -> Result<(), Box<dyn Error>> {
	let trail_path = trail_path();
	{
		let settings = Settings { version: 1 };
		let toml = toml::to_string(&settings)?;
		let settings_path = trail_settings_path(&trail_path);
		let mut file = OpenOptions::new()
			.write(true)
			.create_new(true)
			.open(settings_path)?;
		file.write_all(toml.as_bytes())?;
	}
	{
		let data_path = trail_data_path(&trail_path);
		fs::create_dir_all(data_path)?;
	}
	Ok(())
}
