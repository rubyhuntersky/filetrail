use std::fs;
use std::fs::{File, OpenOptions};
use std::io::{Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};

use crate::core::trail_end::TrailEnd;
use crate::core::TrailError;

pub struct FileTrail {
	base_path: PathBuf,
	trail_end_path: PathBuf,
	trail_end_file: File,
	trail_end: TrailEnd,
}


impl FileTrail {
	pub fn load(base_path: &Path) -> Result<Self, TrailError> {
		let base_path = base_path.to_owned();
		let trail_end_path = base_path.join("End.json");

		let mut trail_end_file = OpenOptions::new()
			.read(true)
			.write(true)
			.create(true)
			.open(&trail_end_path)?;
		let trail_end_string = {
			let mut string = String::new();
			trail_end_file.read_to_string(&mut string)?;
			string
		};
		eprintln!("SAVED: {}", trail_end_string);
		let result = serde_json::from_str::<TrailEnd>(&trail_end_string);
		let trail_end = match result {
			Ok(trail_end) => trail_end,
			Err(_) => {
				let trail_end = TrailEnd { height: 0, content: String::new() };
				write_trail_end(&mut trail_end_file, &trail_end)?;
				trail_end
			}
		};
		let trail = FileTrail { base_path, trail_end_path, trail_end_file, trail_end };
		Ok(trail)
	}

	pub fn write(&mut self, height: usize, content: String) -> Result<usize, TrailError> {
		if height != self.trail_end.height {
			let e = TrailError::InvalidHeight { expected: self.trail_end.height, received: height };
			Err(e)
		} else {
			{
				let archive_path = self.base_path.join(&format!("Height-{:010}.json", &self.trail_end.height));
				fs::copy(&self.trail_end_path, &archive_path)?;
			}
			let trail_end = TrailEnd { height: self.trail_end.height + 1, content: content.to_owned() };
			write_trail_end(&mut self.trail_end_file, &trail_end)?;
			self.trail_end = trail_end;
			Ok(self.trail_end.height)
		}
	}
	pub fn read(&self) -> &TrailEnd { &self.trail_end }
}

fn write_trail_end(trail_end_file: &mut File, trail_end: &TrailEnd) -> Result<(), TrailError> {
	let json = serde_json::to_string(trail_end).expect("trail end to json");
	trail_end_file.seek(SeekFrom::Start(0))?;
	trail_end_file.set_len(0)?;
	trail_end_file.write_all(json.as_bytes())?;
	Ok(())
}

#[cfg(test)]
mod tests {
	use std::fs;

	use rand::random;

	use crate::core::{FileTrail, TrailEnd};

	#[test]
	fn write() {
		let temp = std::env::temp_dir().join(format!("filetrail-test-{}", random::<usize>()));
		fs::create_dir_all(&temp).expect("create dir");
		eprintln!("Path: {:?}", temp);
		let mut trail = FileTrail::load(&temp).expect("trail");
		trail.write(0, "Hello".into()).expect("write");
		trail.write(1, "World!".into()).expect("write");
		assert_eq!(trail.read(), &TrailEnd { height: 2, content: "World!".into() });
	}
}
