use std::collections::HashMap;

use serde_derive::{Deserialize, Serialize};

use crate::core::TrailError;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct TrailEnd {
	pub height: usize,
	pub content: String,
}

impl TryFrom<&HashMap<String, String>> for TrailEnd {
	type Error = TrailError;

	fn try_from(value: &HashMap<String, String>) -> Result<Self, Self::Error> {
		let height = if let Some(s) = value.get("height") {
			s.parse::<usize>()?
		} else { 0 };

		let content = if let Some(s) = value.get("content") {
			s.to_string()
		} else { "".to_string() };

		Ok(TrailEnd { height, content })
	}
}
