use std::{fmt, io};
use std::num::ParseIntError;

#[derive(Debug)]
pub enum TrailError {
	InvalidHeight { expected: usize, received: usize },
	Other(String),
}

impl From<io::Error> for TrailError {
	fn from(e: io::Error) -> Self {
		TrailError::Other(e.to_string())
	}
}

impl From<ParseIntError> for TrailError {
	fn from(e: ParseIntError) -> Self {
		TrailError::Other(e.to_string())
	}
}

impl From<toml::de::Error> for TrailError {
	fn from(e: toml::de::Error) -> Self {
		TrailError::Other(e.to_string())
	}
}

impl From<toml::ser::Error> for TrailError {
	fn from(e: toml::ser::Error) -> Self {
		TrailError::Other(e.to_string())
	}
}

impl fmt::Display for TrailError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self)
	}
}


